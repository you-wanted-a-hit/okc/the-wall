def validate_track(track):
    is_valid = False
    if track:
        is_valid =  'title' in track and        \
                    '_id' in track and          \
                    'duration_ms' in track and  \
                    'album' in track  and       \
                    'artists' in track  and     \
                    'audio_features' in track
    return is_valid

def validate_playlist(playlist):
    is_valid = False
    if playlist:
        is_valid =  'name' in playlist and      \
                    '_id' in playlist and       \
                    'followers' in playlist and \
                    'tracks' in playlist 
        
        if is_valid:
            for track in playlist['tracks']:
                if not validate_track(track):
                    return False
    return is_valid

def validate_album(album):
    is_valid = False
    if album:
        is_valid =  'title' in album and        \
                    '_id' in album and          \
                    'release_date' in album and \
                    'images' in album and       \
                    'artists' in album and      \
                    'tracks' in album

        if is_valid:
            for track in album['tracks']:
                if not validate_track(track):
                    return False
    return is_valid 

def validate_artist(artist):
    is_valid = False
    if artist:
        is_valid =  'name' in artist and    \
                    '_id' in artist and     \
                    'images' in artist and  \
                    'genres' in artist  and \
                    'followers' in artist
    return is_valid

def validate_message(message):
    is_valid = False
    if message.key == 'GET':
        is_valid = '_id' in message.value or 'object' in message.value
    elif message.key == 'POST':
        if message.value.get('type') == 'track':
            is_valid = validate_track(message.value.get('object'))
        elif message.value.get('type') == 'playlist':
            is_valid = validate_playlist(message.value.get('object'))
        elif message.value.get('type') == 'album':
            is_valid = validate_album(message.value.get('object'))
        elif message.value.get('type') == 'artist':
            is_valid = validate_artist(message.value.get('object'))
    elif message.key == "track":
        is_valid = 'object' in message.value

    return is_valid