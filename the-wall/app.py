# Custom Modules
from consumer.consumer import Consumer
from producer.producer import Producer
from log import log
from utils import utils
from manager import manager
from db import database

# Built-in Modules
import threading
import queue

# External Dependencies
from decouple import config

# Instanciate task queue
tasks = queue.Queue()

def consume_daemon():
    logger = log.get_logger('C-DAEMON')
    
    logger.info('Setting up consumer')

    # Setting up consumer
    host = config('KAFKA_HOST')
    port = config('KAFKA_PORT')

    consumer_topic = config('KAFKA_CONSUMER_TOPIC')
    consumer = Consumer(host=host, port=port, topics=consumer_topic, consumer_group=config('KAFKA_CONSUMER_GROUP')).consumer
    
    logger.info('Completed setup stage')

    for message in consumer:
        logger.info(f'New message received {message.key} -> {message.value}')
        tasks.put(message)
        logger.info(f'New message is in queue, waiting to be proccessed')

def run():
    logger = log.get_logger('PROD LOOP')

    # Setting producer instance
    host = config('KAFKA_HOST')
    port = config('KAFKA_PORT')
    producer_topic = config('KAFKA_PRODUCER_TOPIC')
    producer = Producer(host=host, port=port)

    db_name = config('MONGODB_DATABASE_NAME')
    uri = f'mongodb://{config("MONGODB_ROOT_USER")}:{config("MONGODB_ROOT_PASSWORD")}@{config("MONGODB_HOST")}:{config("MONGODB_PORT")}'
    db = database.Database(name=db_name, uri=uri)

    # Launching consumer daemon
    threading.Thread(target=consume_daemon, daemon=True).start()
    logger.info('consumer thread is running')

    while True:
        logger.info('waiting for new task')
        message = tasks.get()
        # if utils.validate_message(message):
        logger.info('Message is in valid format')
        # logger.info(f'Doing task {message.key} {message.value["type"]} {message.value["object"]["_id"]}')
        response = manager.do_action(db=db, action=message.key, resource_type=message.value['type'], params=message.value["object"])
        # logger.info(f'Completed task {message.key} {message.value["type"]} {message.value["object"]["_id"]}')
        logger.info(f'Publishing response')
        if response:
            logger.debug(response)
            if message.key == 'GET':
                if message.value.get('crons') == True:
                    logger.debug('MESSAGE FROM CRON')
                    producer.publish(key=message.key, message=response, topic=config("KAFKA_CRONS_TOPIC"))
                else:
                    producer.publish(key=message.key, message=response, topic=producer_topic)
            elif message.key == 'POST':
                producer.publish(key=message.key, message=response, topic=config("KAFKA_POST_TOPIC"))

        else:
            logger.info('No asset found')
            if message.value.get('crons') == True:
                logger.debug('MESSAGE FROM CRON')
                producer.publish(key=message.key, message=response, topic=config("KAFKA_CRONS_TOPIC"))
            else:
                producer.publish(key=message.key, message=response, topic=producer_topic)
        # else:
        #     err_json = {
        #         'request'   : message.value,
        #         'error_msg' : 'The sent message is not in a valid format'
        #     }
        #     producer.publish(message.key, err_json, producer_topic)
        tasks.task_done()