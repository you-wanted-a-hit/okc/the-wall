def do_action(db, action, resource_type, params):
    resource = None
    if action == 'GET':
        if resource_type == 'track':
            if 'title' in params:
                resource = db.getTrackByName(song_name=params['title'],artist=params['artist'])
            elif "_id" in params:
                resource = db.getTrack(spotify_id=params['_id'])
        elif resource_type == 'album':
            resource = db.getAlbum(spotify_id=params['_id'])
        elif resource_type == 'artist':
            resource = db.getArtist(spotify_id=params['_id'])
        elif resource_type == 'playlist':
            resource = db.getPlaylist(spotify_id=params['_id'])
        elif resource_type == 'chart':
            resource = db.getChart(date=params['date'])
        elif resource_type == 'partials':
            resource = db.collectPartials()
        elif resource_type == 'closest':
            resource = db.findClosestTracks(spotify_id=params['_id'])
    if action == 'POST':
        if resource_type == 'track':
            resource = db.insertTrack(params)
        elif resource_type == 'album':
            resource = db.insertAlbum(params)
        elif resource_type == 'artist':
            resource = db.insertArtist(params)
        elif resource_type == 'playlist':
            resource = db.insertPlaylist(params)
        elif resource_type == 'chart':
            resource = db.insertChart(params)
 
    return create_response(resource)


def create_response(resource):
    if resource:
        if resource == 200:
            return {'status': 200}
        if resource.__class__ == dict:
            return resource
        return resource.serialize()
