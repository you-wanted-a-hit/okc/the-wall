class PartialTrack:
    def __init__(self, id):
        # Extracting info
        self.title = None
        self._id = id
        self.duration_ms = None
        self.preview_url = None
        self.track_number = None
        self.album_id = None
        # Extracting artist
        self.artists = None
        self.audio_features = None
        self.top_rank = 1000
        self.num_of_weeks = 0
        self.isPartial = True

    def __repr__(self):
        return self.serialize()

    def serialize(self):
        return self.__dict__
