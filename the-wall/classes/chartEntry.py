class ChartEntry:
    def __init__(self,info):
        if "_id" not in info:
            self.trackId = None
        else:
            self.trackId = info["_id"]
            self.peak_pos = info["peak_pos"]
            self.number_of_weeks = info["weeks"]

    def __repr__(self):
        return str(self.__dict__)