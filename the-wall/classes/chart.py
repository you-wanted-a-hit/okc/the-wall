from classes import chartEntry


class Chart:
    def __init__(self, info):
        self.date = info["date"]
        self._id = str(hash(str(self.date)))[1:13]
        self.entries = []
        self.entries = self.buildEntries(info['entries'])

    def __repr__(self):
        return str(self.__dict__)

    def buildEntries(self, array):
        temp = []
        for i in range(len(array)):
            entry = chartEntry.ChartEntry(array[i])
            if entry.trackId is not None:
                temp.append(entry.__dict__)
        filteredArray = []
        for _ in range(len(temp)):
            if temp[_] is not None:
                filteredArray.append(temp[_])
        return filteredArray