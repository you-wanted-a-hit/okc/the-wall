class Album:
    def __init__(self, info):
        self.title = info['title']
        self._id = info['_id']
        self.release_date = info['release_date']
        self.images = info['images']

        self.artists = []
        for artist in info['artists']:
            self.artists.append(artist['_id'])

        self.tracks = []
        for track_json in info['tracks']:
            track_id = track_json["_id"]
            self.tracks.append(track_id)
        self.isPartial = False

    def __repr__(self):
        return self.serialize()

    def serialize(self):
        return self.__dict__
