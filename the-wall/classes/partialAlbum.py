class PartialAlbum:
    def __init__(self, id,name):
        self.title = name
        self._id = id
        self.release_date = None
        self.images = None
        self.artists = None
        self.tracks = None
        self.isPartial = True

    def __repr__(self):
        return self.serialize()

    def serialize(self):
        return self.__dict__
