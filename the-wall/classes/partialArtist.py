class PartialArtist:
    def __init__(self, id,name):
        self.name = name
        self.lower_name = str(name).lower()
        self._id = id
        self.images = None
        self.genres = None
        self.followers = None
        self.isPartial = True

    def __repr__(self):
        return self.serialize()

    def serialize(self):
        return self.__dict__
