class Artist:
    def __init__(self, info):
        self.name = info['name']
        self.lower_name = str(info['name']).lower()
        self._id = info['_id']
        self.images = info['images']
        self.genres = info['genres']
        self.followers = info['followers']
        self.isPartial = False

    def __repr__(self):
        return self.serialize()

    def serialize(self):
        return self.__dict__
