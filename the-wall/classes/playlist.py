class Playlist:
    def __init__(self, info):
        self.name = info['name']
        self._id = info['_id']
        self.followers = info['followers']

        self.tracks = []
        for track_json in info['tracks']:
            track_id = track_json["_id"]
            self.tracks.append(track_id)

    def __repr__(self):
        return self.serialize()

    def serialize(self):
        return self.__dict__
