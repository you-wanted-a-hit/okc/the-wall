class Track:
    def __init__(self, info):
        # Extracting info
        self.title = info['title']
        self.lower_title = str(info['title']).lower()
        self._id = info['_id']
        self.duration_ms = info['duration_ms']
        self.preview_url = info['preview_url']
        self.track_number = info['track_number']
        self.album_id = info['album']['_id']
        # Extracting artist
        self.artists = []
        for artist in info['artists']:
            self.artists.append(artist['_id'])
        self.add_audio_features(info["audio_features"])
        self.top_rank = 1000
        self.num_of_weeks = 0
        self.isPartial = False

    def __repr__(self):
        return self.serialize()

    def add_audio_features(self, audio_features):
        # Extracting audio features
        self.audio_features = {
            'danceability': audio_features['danceability'],
            'energy': audio_features['energy'],
            'key': audio_features['key'],
            'loudness': audio_features['loudness'],
            'mode': audio_features['mode'],
            'speechiness': audio_features['speechiness'],
            'acousticness': audio_features['acousticness'],
            'instrumentalness': audio_features['instrumentalness'],
            'liveness': audio_features['liveness'],
            'valence': audio_features['valence'],
            'tempo': audio_features['tempo'],
            'duration_ms': audio_features['duration_ms'],
            'time_signature': audio_features['time_signature']
        }

    def serialize(self):
        return self.__dict__
