import json
from pymongo import MongoClient
from pprint import pprint
from scipy.spatial import distance
import numpy as np
from classes import album, artist, playlist, track, partialTrack, partialAlbum ,partialArtist, chart


class Database:
    def __init__(self, name, uri):
        client = MongoClient(uri)
        self.database = client.get_database(name=name)
        if (self.database == None):
            self.database = client[name]

        serverStatusResult = self.database.command("serverStatus")
        pprint(serverStatusResult)

    def insertTrack(self, info):
        trackid = info["_id"]
        if self.isTrackInDB(trackid):
            if self.isTrackPartial(trackid):
                self.fillTrack(info)
                return
            else:
                return
        new_track = track.Track(info)
        # check if already in the database
        if (new_track != None):
            try:
                self.database.tracks.insert_one(new_track.serialize())
            except Exception as ex:
                print(f"{new_track.title} is already in the database")
            for _ in info["artists"]:
                if self.isArtistInDB(_["_id"]):
                    continue
                new_artist = partialArtist.PartialArtist(_["_id"],_["name"])
                if new_artist != None:
                    try:
                        self.database.artists.insert_one(new_artist.serialize())
                    except Exception as ex:
                        print(f"The artist {new_artist._id} is already in the database")
            if new_track.album_id != None and not self.isAlbumInDB(new_track.album_id):
                new_album = partialAlbum.PartialAlbum(new_track.album_id,info["album"]["name"])
                try:
                    self.database.albums.insert_one(new_album.serialize())
                except Exception as ex:
                    print(f"The artist {new_album._id} is already in the database")
        return 200

    def insertPlaylist(self, info):
        if self.isPlaylistInDB(info["_id"]):
            return
        new_playlist = playlist.Playlist(info)
        if (new_playlist != None):
            for tr in new_playlist.tracks:
                if not self.isTrackInDB(tr):
                    new_track = partialTrack.PartialTrack(tr)
                    try:
                        self.database.tracks.insert_one(new_track.serialize())
                    except Exception as ex:
                        print(f"{new_track._id} is already in the database")

            try:
                self.database.playlists.insert_one(new_playlist.serialize())
            except Exception as ex:
                print(f"The playlist {new_playlist.name} is already in the database")
        return 200

    def insertChart(self, info):
        chartid = str(hash(str(info["date"])))[1:13]
        if self.isChartInDB(chartid):
            return
        new_chart = chart.Chart(info)
        if (new_chart != None):
            for en in new_chart.entries:
                id = en["trackId"]
                if not self.isTrackInDB(id):
                    new_track = partialTrack.PartialTrack(id)
                    new_track.top_rank = en["peak_pos"]
                    new_track.num_of_weeks = en["number_of_weeks"]
                    try:
                        self.database.tracks.insert_one(new_track.serialize())
                    except Exception as ex:
                        print(f"{id} is already in the database")
                else:
                    song = self.database.tracks.find_one({"_id": id})
                    song_rank =  song["top_rank"]
                    song_weeks =  song["num_of_weeks"]
                    current_entry_rank = en["peak_pos"]
                    current_entry_weeks = en["number_of_weeks"]
                    myquery = {"_id": id}
                    if song_rank < current_entry_rank and song_weeks > current_entry_weeks:
                        newvalues = {"$set": {"top_rank": en["peak_pos"], "num_of_weeks": en["number_of_weeks"]}}
                    elif song_rank < current_entry_rank:
                        newvalues = {"$set": {"top_rank": en["peak_pos"]}}
                    elif song_weeks > current_entry_weeks:
                        newvalues = {"$set": {"num_of_weeks": en["number_of_weeks"]}}
                    else:
                        return
                    self.database.tracks.update_one(myquery, newvalues)

            try:
                self.database.charts.insert_one(new_chart.__dict__)
            except Exception as ex:
                print(f'The chart of {new_chart.date} is already in the database')

        return 200

    def insertAlbum(self, info):
        albumid = info["_id"]
        if self.isAlbumInDB(albumid):
            if self.isAlbumPartial(albumid):
                self.fillAlbum(info)
                return
            else:
                return
        new_album = album.Album(info)
        if (new_album != None):
            for tr in new_album.tracks:
                if not self.isTrackInDB(tr):
                    new_track = partialTrack.PartialTrack(tr)
                    try:
                        self.database.tracks.insert_one(new_track.serialize())
                    except Exception as ex:
                        print(f"{new_track._id} is already in the database")

            try:
                self.database.albums.insert_one(new_album.serialize())
            except Exception as ex:
                print(f"The album {new_album.title} is already in the database")
        return 200

    def insertArtist(self, info):
        aritstid = info["_id"]
        if self.isArtistInDB(aritstid):
            if self.isArtistPartial(aritstid):
                self.fillArtist(info)
                return
            else:
                return
        new_artist = artist.Artist(info)
        if (new_artist != None):
            try:
                self.database.artists.insert_one(new_artist.serialize())
            except Exception as ex:
                print(f"The artist {new_artist.name} is already in the database")
        return 200

    def isTrackInDB(self, id):
        song = self.database.tracks.find_one({"_id": id})
        if song != None:
            return True
        return False

    def isChartInDB(self, id):
        chart = self.database.charts.find_one({"_id": id})
        if chart != None:
            return True
        return False

    def isAlbumInDB(self, id):
        Album = self.database.albums.find_one({"_id": id})
        if Album != None:
            return True
        return False

    def isPlaylistInDB(self, id):
        playlist = self.database.playlists.find_one({"_id": id})
        if playlist != None:
            return True
        return False

    def isArtistInDB(self, id):
        artist = self.database.artists.find_one({"_id": id})
        if artist != None:
            return True
        return False

    def isTrackPartial(self, id):
        song = self.database.tracks.find_one({"_id": id})
        return song["isPartial"]

    def isAlbumPartial(self, id):
        Album = self.database.albums.find_one({"_id": id})
        return Album["isPartial"]

    def isArtistPartial(self, id):
        artist = self.database.artists.find_one({"_id": id})
        return artist["isPartial"]

    def fillTrack(self,info):
        myquery = {"_id": info["_id"]}
        t_title = info['title']
        t_duration_ms = info['duration_ms']
        t_preview_url = info['preview_url']
        t_track_number = info['track_number']
        t_album_id = info['album']['_id']
        # Extracting artist
        t_artists = []
        for artist in info['artists']:
            t_artists.append(artist['_id'])
        audio_features = info["audio_features"]
        t_audio_features = {
            'danceability': audio_features['danceability'],
            'energy': audio_features['energy'],
            'key': audio_features['key'],
            'loudness': audio_features['loudness'],
            'mode': audio_features['mode'],
            'speechiness': audio_features['speechiness'],
            'acousticness': audio_features['acousticness'],
            'instrumentalness': audio_features['instrumentalness'],
            'liveness': audio_features['liveness'],
            'valence': audio_features['valence'],
            'tempo': audio_features['tempo'],
            'duration_ms': audio_features['duration_ms'],
            'time_signature': audio_features['time_signature']
        }
        newvalues = {"$set": {"title": t_title,
                              "duration_ms": t_duration_ms,
                              "preview_url": t_preview_url,
                              "track_number": t_track_number,
                              "album_id": t_album_id,
                              "artists": t_artists,
                              "audio_features": t_audio_features,
                              "isPartial": False
                              }}

        for _ in t_artists:
            if self.isArtistInDB(_):
                continue
            new_artist = partialArtist.PartialArtist(_)
            if new_artist != None:
                try:
                    self.database.artists.insert_one(new_artist.serialize())
                except Exception as ex:
                    print(f"The artist {new_artist._id} is already in the database")

        self.database.tracks.update_one(myquery, newvalues)

    def fillAlbum(self,info):
        myquery = {"_id": info["_id"]}
        t_title = info['title']
        t_release_date = info['release_date']
        t_images = info['images']
        # Extracting artist
        t_artists = []
        for artist in info['artists']:
            t_artists.append(artist['_id'])
        t_tracks = []
        for track_json in info['tracks']:
            track_id = track_json["_id"]
            t_tracks.append(track_id)

        newvalues = {"$set": {"title": t_title,
                              "release_date": t_release_date,
                              "images": t_images,
                              "artists": t_artists,
                              "tracks": t_tracks,
                              "isPartial": False
                              }}

        self.database.albums.update_one(myquery, newvalues)

    def fillArtist(self, info):
        myquery = {"_id": info["_id"]}
        t_name = info['name']
        t_images = info['images']
        t_genres = info['genres']
        t_followers = info['followers']

        newvalues = {"$set": {"name": t_name,
                              "images": t_images,
                              "genres": t_genres,
                              "followers": t_followers,
                              "isPartial": False
                              }}

        self.database.artists.update_one(myquery, newvalues)

    def collectPartials(self):
        parTracks = self.collectPartialTracks()
        parAlbums = self.collectPartialAlbums()
        parArtists = self.collectPartialArtists()
        return {"tracks":parTracks,"albums":parAlbums,"artists":parArtists}

    def collectPartialTracks(self):
        myquery = {"isPartial": True}
        tracks = []
        for track in self.database.tracks.find(myquery, {"_id": 1}):
            tracks.append(track["_id"])

        return tracks

    def collectPartialAlbums(self):
        myquery = {"isPartial": True}
        albums = []
        for album in self.database.albums.find(myquery, {"_id": 1}):
            albums.append(album["_id"])
        return albums

    def collectPartialArtists(self):
        myquery = {"isPartial": True}
        artists = []
        for artist in self.database.artists.find(myquery, {"_id": 1}):
            artists.append(artist["_id"])
        return artists

    def fillInTracks(self,info):
        for _ in info:
            self.insertTrack(info)

    def fillInTracks(self,info):
        for _ in info:
            self.insertTrack(info)

    def fillInTracks(self,info):
        for _ in info:
            self.insertTrack(info)

    def getAllTracks(self):
        myquery = {"isPartial": False}
        song = self.database.tracks.find(myquery,{"_id": 1, "title":1, "audio_features":1})
        return song

    def extractAudioFeatures(self, audio_features):
        features = {
            'danceability': audio_features['danceability'],
            'energy': audio_features['energy'],
            'key': audio_features['key'],
            'loudness': audio_features['loudness'],
            'mode': audio_features['mode'],
            'speechiness': audio_features['speechiness'],
            'acousticness': audio_features['acousticness'],
            'instrumentalness': audio_features['instrumentalness'],
            'liveness': audio_features['liveness'],
            'valence': audio_features['valence'],
            'tempo': audio_features['tempo'],
            'duration_ms': audio_features['duration_ms'],
            'time_signature': audio_features['time_signature']
        }
        featuresArray = []
        for i in features:
            featuresArray.append(features[i])
        return featuresArray

    def findClosestTracks(self,spotify_id):
        allTracks = self.getAllTracks()
        allSongs = []
        IDMappingToDistanceMapping = dict()
        song = self.getTrack(spotify_id)
        songs_features = self.extractAudioFeatures(song["audio_features"])
        distances = []
        results = []
        for i in allTracks:
            audio_f = self.extractAudioFeatures(i["audio_features"])
            dst = distance.euclidean(songs_features, audio_f)
            if dst == 0.0:
                continue
            song_id = i["_id"]
            IDMappingToDistanceMapping[song_id] = dst
            distances.append(dst)
        distances = np.partition(distances,10)
        distances = distances[:10]

        for key,val in IDMappingToDistanceMapping.items():
            if val in distances:
                allSongs.append(key)

        for trackID in allSongs:
            t = self.getTrack(trackID)
            if t:
                results.append(t)

        return {"tracks":results}
    
    def getTrack(self, spotify_id):
        song = self.database.tracks.find_one({"_id": spotify_id,"isPartial":False})
        if song:
            artists = song["artists"]
            listOfArtists = []
            for i in artists:
                artist = self.get_artist(i)
                listOfArtists.append(artist)
                # else:
                #     listOfArtists.append({"_id": i})
            song["artists"] = listOfArtists

            song["album"]= self.get_album(song["album_id"])
        return song

    # Regardless if partial or not
    # def get_track(self, _id):
    #     track = self.database.tracks.find_one({"_id": _id})

    def getTrackByName(self, song_name, artist):
        song = self.database.tracks.find_one({"lower_title": song_name.lower()})
        if (song != None):
            artists_obj = []
            artists = song["artists"]
            for art in artists:
                artist_obj = self.get_artist(art)
                artists_obj.append(artist_obj)
            song["artists"] = artists_obj
            for art in song["artists"]:
                if art:
                    if (artist.lower() == art["lower_name"]):
                        song['album'] = self.get_album(song['album_id'])
                        return song
        else:
            return None

    def getArtist(self, spotify_id):
        return self.database.artists.find_one({"_id": spotify_id,"isPartial": False})
    
    # Regardless of partial or not
    def get_artist(self, _id):
        return self.database.artists.find_one({"_id": _id})

    def getPlaylist(self, spotify_id):
        plist = self.database.playlists.find_one({"_id": spotify_id})
        tracks = []
        for tr in plist["tracks"]:
            track_obj = self.getTrack(tr)
            tracks.append(track_obj)
        plist["tracks"] = tracks
        return plist

    def getAlbum(self, spotify_id):
        album = self.database.albums.find_one({"_id": spotify_id,"isPartial": False})
        return album

    # Regardless of partial or not
    def get_album(self, _id):
        return self.database.albums.find_one({"_id": _id})

    def getChart(self, date):
        chart = self.database.charts.find_one({"date": date})
        for tr in chart["entries"]["track"]:
            track_obj = self.getTrack(tr)
            tr = track_obj
        return chart

    def getPlaylistsWithTrack(self, spotify_id):
        playlists = self.database.playlists.find({"tracks": [spotify_id]})
        return playlists

