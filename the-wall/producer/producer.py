import json
from kafka import KafkaProducer

def serialize_value(msg):
    if msg and msg.__class__ != dict:
        return msg.serialize()
    return msg

class Producer:
    def __init__(self, host, port):
        self.producer = KafkaProducer(
            bootstrap_servers=[f'{host}:{port}'],
            value_serializer= lambda msg: json.dumps(serialize_value(msg)).encode('utf-8'),
            key_serializer= lambda key: key.encode('utf-8'))
        
    def publish(self, key, message, topic):
        self.producer.send(topic, message, key=key)

