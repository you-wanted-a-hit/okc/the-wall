import app
from pymongo import MongoClient
from pprint import pprint
from db import database
import json


def test():
    db_name = "t"
    uri = 'mongodb://root:example@localhost:27017'
    db = database.Database(name=db_name, uri=uri)

    chart = json.loads('''{
               "title": "The Hot 100",
               "name": "hot-100",
               "date": "2009-05-23",
               "next_date": "2009-05-30",
               "prev_date": "2009-05-16",
               "entries": [
                   {
                       "title": "Crazy In Love (feat. Jay-Z)",
                       "is_new": false,
                       "last_pos": 1,
                       "peak_pos": 1,
                       "rank": 1,
                       "weeks": 9,
                       "_id": "0TwBtDAWpkpM3srywFVOV5"
                   },
                   {
                       "title": "Poker Face",
                       "artist": "Lady Gaga",
                       "is_new": false,
                       "last_pos": 3,
                       "peak_pos": 1,
                       "rank": 2,
                       "weeks": 21,
                       "_id": "5R8dQOPq8haW94K7mgERlO"
                   },
                   {
                       "title": "Blame It",
                       "artist": "Jamie Foxx Featuring T-Pain",
                       "is_new": false,
                       "last_pos": 2,
                       "peak_pos": 2,
                       "rank": 3,
                       "weeks": 17
                   },
                   {
                       "title": "Day 'N' Nite",
                       "artist": "Kid Cudi",
                       "is_new": false,
                       "last_pos": 4,
                       "peak_pos": 3,
                       "rank": 4,
                       "weeks": 17,
                       "_id": "5FEXPoPnzueFJQCPRIrC3c"
                   },
                   {
                       "title": "Halo",
                       "artist": "Beyonce",
                       "is_new": false,
                       "last_pos": 8,
                       "peak_pos": 5,
                       "rank": 5,
                       "weeks": 16,
                       "_id": "4JehYebiI9JE8sR8MisGVb"
                   },
                   {
                       "title": "Sugar",
                       "artist": "Flo Rida Featuring Wynter",
                       "is_new": false,
                       "last_pos": 5,
                       "peak_pos": 5,
                       "rank": 6,
                       "weeks": 8
                   },
                   {
                       "title": "Kiss Me Thru The Phone",
                       "artist": "Soulja Boy Tell 'em Featuring Sammie",
                       "is_new": false,
                       "last_pos": 7,
                       "peak_pos": 3,
                       "rank": 7,
                       "weeks": 20
                   },
                   {
                       "title": "Don't Trust Me",
                       "artist": "3OH!3",
                       "is_new": false,
                       "last_pos": 9,
                       "peak_pos": 8,
                       "rank": 8,
                       "weeks": 22,
                       "_id": "5jzX4dWVQeBTtfBaXnMRt5"
                   },
                   {
                       "title": "Right Round",
                       "artist": "Flo Rida",
                       "is_new": false,
                       "last_pos": 6,
                       "peak_pos": 1,
                       "rank": 9,
                       "weeks": 15,
                       "_id": "3GpbwCm3YxiWDvy29Uo3vP"
                   },
                   {
                       "title": "The Climb",
                       "artist": "Miley Cyrus",
                       "is_new": false,
                       "last_pos": 11,
                       "peak_pos": 4,
                       "rank": 10,
                       "weeks": 10,
                       "_id": "0V8FYVlBFuXXTIvRnMbZyS"
                   },
                   {
                       "title": "I Know You Want Me (Calle Ocho)",
                       "artist": "Pitbull",
                       "is_new": false,
                       "last_pos": 12,
                       "peak_pos": 11,
                       "rank": 11,
                       "weeks": 11,
                       "_id": "13plQdOoWSSXPRUSZc5FuM"
                   },
                   {
                       "title": "Dead And Gone",
                       "artist": "T.I. Featuring Justin Timberlake",
                       "is_new": false,
                       "last_pos": 10,
                       "peak_pos": 2,
                       "rank": 12,
                       "weeks": 23,
                       "_id": "1o7CxicWDmkZAVxlcldFpc"
                   },
                   {
                       "title": "Birthday Sex",
                       "artist": "Jeremih",
                       "is_new": false,
                       "last_pos": 23,
                       "peak_pos": 13,
                       "rank": 13,
                       "weeks": 5,
                       "_id": "4NpDZPwSXmL0cCTaJuVrCw"
                   },
                   {
                       "title": "Second Chance",
                       "artist": "Shinedown",
                       "is_new": false,
                       "last_pos": 15,
                       "peak_pos": 14,
                       "rank": 14,
                       "weeks": 24,
                       "_id": "0CwYG1UnRmOx8Q1EzElCIL"
                   },
                   {
                       "title": "No Surprise",
                       "artist": "Daughtry",
                       "is_new": true,
                       "last_pos": 0,
                       "peak_pos": 15,
                       "rank": 15,
                       "weeks": 1,
                       "_id": "6q4XLE5WeOBWftoc9gTl2P"
                   },
                   {
                       "title": "Gives You Hell",
                       "artist": "The All-American Rejects",
                       "is_new": false,
                       "last_pos": 14,
                       "peak_pos": 4,
                       "rank": 16,
                       "weeks": 26,
                       "_id": "6ihL9TjfRjadfEePzXXyVF"
                   },
                   {
                       "title": "Knock You Down",
                       "artist": "Keri Hilson Featuring Kanye West & Ne-Yo",
                       "is_new": false,
                       "last_pos": 22,
                       "peak_pos": 17,
                       "rank": 17,
                       "weeks": 7,
                       "_id": "6DXlxBjG5Na4f4KI8a0y6F"
                   },
                   {
                       "title": "My Life Would Suck Without You",
                       "artist": "Kelly Clarkson",
                       "is_new": false,
                       "last_pos": 16,
                       "peak_pos": 1,
                       "rank": 18,
                       "weeks": 17,
                       "_id": "4Dm32oO01YpIubCHaAtKkN"
                   },
                   {
                       "title": "Just Dance",
                       "artist": "Lady Gaga Featuring Colby O'Donis",
                       "is_new": false,
                       "last_pos": 17,
                       "peak_pos": 1,
                       "rank": 19,
                       "weeks": 40
                   },
                   {
                       "title": "Turn My Swag On",
                       "artist": "Soulja Boy Tell'em",
                       "is_new": false,
                       "last_pos": 19,
                       "peak_pos": 19,
                       "rank": 20,
                       "weeks": 10,
                       "_id": "6ATrsVaZT7XjkCynxM8cTS"
                   },
                   {
                       "title": "Love Story",
                       "artist": "Taylor Swift",
                       "is_new": false,
                       "last_pos": 18,
                       "peak_pos": 4,
                       "rank": 21,
                       "weeks": 35,
                       "_id": "3CeCwYWvdfXbZLXFhBrbnf"
                   },
                   {
                       "title": "We Made You",
                       "artist": "Eminem",
                       "is_new": false,
                       "last_pos": 13,
                       "peak_pos": 9,
                       "rank": 22,
                       "weeks": 4,
                       "_id": "4UMTp91LHhvW33ol9ZQH0Q"
                   },
                   {
                       "title": "You Found Me",
                       "artist": "The Fray",
                       "is_new": false,
                       "last_pos": 20,
                       "peak_pos": 7,
                       "rank": 23,
                       "weeks": 25,
                       "_id": "4IoYz8XqqdowINzfRrFnhi"
                   },
                   {
                       "title": "If U Seek Amy",
                       "artist": "Britney Spears",
                       "is_new": false,
                       "last_pos": 21,
                       "peak_pos": 19,
                       "rank": 24,
                       "weeks": 13,
                       "_id": "2hdy9Wt9qp7M7d0U3ossu2"
                   },
                   {
                       "title": "Old Time's Sake",
                       "artist": "Eminem Featuring Dr. Dre",
                       "is_new": true,
                       "last_pos": 0,
                       "peak_pos": 25,
                       "rank": 25,
                       "weeks": 1
                   },
                   {
                       "title": "I Do Not Hook Up",
                       "artist": "Kelly Clarkson",
                       "is_new": false,
                       "last_pos": 24,
                       "peak_pos": 24,
                       "rank": 26,
                       "weeks": 4,
                       "_id": "2I5CPUnMIKjEXeg61OI9uV"
                   },
                   {
                       "title": "I'm Yours",
                       "artist": "Jason Mraz",
                       "is_new": false,
                       "last_pos": 25,
                       "peak_pos": 6,
                       "rank": 27,
                       "weeks": 56,
                       "_id": "1EzrEOXmMH3G43AXT1y7pA"
                   },
                   {
                       "title": "Then",
                       "artist": "Brad Paisley",
                       "is_new": false,
                       "last_pos": 31,
                       "peak_pos": 28,
                       "rank": 28,
                       "weeks": 8,
                       "_id": "3XKbdb9GB6u3hsnUklQTav"
                   },
                   {
                       "title": "Fire Burning",
                       "artist": "Sean Kingston",
                       "is_new": true,
                       "last_pos": 0,
                       "peak_pos": 29,
                       "rank": 29,
                       "weeks": 1,
                       "_id": "2oENJa1T33GJ0w8dC167G4"
                   },
                   {
                       "title": "Goodbye",
                       "artist": "Kristinia DeBarge",
                       "is_new": false,
                       "last_pos": 42,
                       "peak_pos": 30,
                       "rank": 30,
                       "weeks": 3,
                       "_id": "5myYDbAurm1CW038qd4gL2"
                   },
                   {
                       "title": "She's Country",
                       "artist": "Jason Aldean",
                       "is_new": false,
                       "last_pos": 29,
                       "peak_pos": 29,
                       "rank": 31,
                       "weeks": 17,
                       "_id": "0PspNrE4MSjr1JeB4OW0jn"
                   },
                   {
                       "title": "How Do You Sleep?",
                       "artist": "Jesse McCartney Featuring Ludacris",
                       "is_new": false,
                       "last_pos": 26,
                       "peak_pos": 26,
                       "rank": 32,
                       "weeks": 14
                   },
                   {
                       "title": "It Happens",
                       "artist": "Sugarland",
                       "is_new": false,
                       "last_pos": 38,
                       "peak_pos": 33,
                       "rank": 33,
                       "weeks": 10,
                       "_id": "1I66zFfRohnb8kwJV3vrV4"
                   },
                   {
                       "title": "1, 2, 3, 4",
                       "artist": "Plain White T's",
                       "is_new": false,
                       "last_pos": 35,
                       "peak_pos": 34,
                       "rank": 34,
                       "weeks": 16,
                       "_id": "5VWmMZCfJ4yVkJw9ZLFXej"
                   },
                   {
                       "title": "Mad",
                       "artist": "Ne-Yo",
                       "is_new": false,
                       "last_pos": 30,
                       "peak_pos": 11,
                       "rank": 35,
                       "weeks": 23,
                       "_id": "6Hn9Uc1mMNfqChXU3txNke"
                   },
                   {
                       "title": "Turnin Me On",
                       "artist": "Keri Hilson Featuring Lil Wayne",
                       "is_new": false,
                       "last_pos": 27,
                       "peak_pos": 15,
                       "rank": 36,
                       "weeks": 21
                   },
                   {
                       "title": "Sober",
                       "artist": "P!nk",
                       "is_new": false,
                       "last_pos": 28,
                       "peak_pos": 15,
                       "rank": 37,
                       "weeks": 24,
                       "_id": "733ncRLzZQT5UtSPg1QmPc"
                   },
                   {
                       "title": "If Today Was Your Last Day",
                       "artist": "Nickelback",
                       "is_new": false,
                       "last_pos": 44,
                       "peak_pos": 35,
                       "rank": 38,
                       "weeks": 8,
                       "_id": "4QJLKU75Rg4558f4LbDBRi"
                   },
                   {
                       "title": "Whatever It Is",
                       "artist": "Zac Brown Band",
                       "is_new": false,
                       "last_pos": 43,
                       "peak_pos": 39,
                       "rank": 39,
                       "weeks": 9,
                       "_id": "2ISojTv3VUnimEOiLddIw4"
                   },
                   {
                       "title": "I Told You So",
                       "artist": "Carrie Underwood Featuring Randy Travis",
                       "is_new": false,
                       "last_pos": 34,
                       "peak_pos": 9,
                       "rank": 40,
                       "weeks": 13
                   },
                   {
                       "title": "Know Your Enemy",
                       "artist": "Green Day",
                       "is_new": false,
                       "last_pos": 46,
                       "peak_pos": 40,
                       "rank": 41,
                       "weeks": 4,
                       "_id": "5qtwzv99vOr5UTwnTixn7j"
                   },
                   {
                       "title": "Boyfriend #2",
                       "artist": "Pleasure P",
                       "is_new": false,
                       "last_pos": 49,
                       "peak_pos": 42,
                       "rank": 42,
                       "weeks": 8,
                       "_id": "3taIcgVjePRzkdz0ZmJbOi"
                   },
                   {
                       "title": "Please Don't Leave Me",
                       "artist": "P!nk",
                       "is_new": false,
                       "last_pos": 51,
                       "peak_pos": 43,
                       "rank": 43,
                       "weeks": 4,
                       "_id": "2gnZad0pb6QtUqMLTj2AjI"
                   },
                   {
                       "title": "Love Sex Magic",
                       "artist": "Ciara Featuring Justin Timberlake",
                       "is_new": false,
                       "last_pos": 37,
                       "peak_pos": 10,
                       "rank": 44,
                       "weeks": 9,
                       "_id": "158q3fa4ZMoMYWdiYWD5NL"
                   },
                   {
                       "title": "Heartless",
                       "artist": "Kanye West",
                       "is_new": false,
                       "last_pos": 33,
                       "peak_pos": 2,
                       "rank": 45,
                       "weeks": 27,
                       "_id": "4EWCNWgDS8707fNSZ1oaA5"
                   },
                   {
                       "title": "Rockin' That Thang",
                       "artist": "The-Dream",
                       "is_new": false,
                       "last_pos": 41,
                       "peak_pos": 22,
                       "rank": 46,
                       "weeks": 20,
                       "_id": "3oJJ1dQBxiFYGMQSe2SluG"
                   },
                   {
                       "title": "I Run To You",
                       "artist": "Lady Antebellum",
                       "is_new": false,
                       "last_pos": 50,
                       "peak_pos": 47,
                       "rank": 47,
                       "weeks": 8,
                       "_id": "2YVCkTYJpv5jelYrHYoEG8"
                   },
                   {
                       "title": "Here Comes Goodbye",
                       "artist": "Rascal Flatts",
                       "is_new": false,
                       "last_pos": 36,
                       "peak_pos": 11,
                       "rank": 48,
                       "weeks": 11,
                       "_id": "4lhajjgcICdfhRkvWj9Tud"
                   },
                   {
                       "title": "Sideways",
                       "artist": "Dierks Bentley",
                       "is_new": false,
                       "last_pos": 52,
                       "peak_pos": 49,
                       "rank": 49,
                       "weeks": 6,
                       "_id": "19DnL1HnJiijuEfaiS0Eus"
                   },
                   {
                       "title": "You Belong With Me",
                       "artist": "Taylor Swift",
                       "is_new": false,
                       "last_pos": 87,
                       "peak_pos": 12,
                       "rank": 50,
                       "weeks": 3,
                       "_id": "3GCL1PydwsLodcpv0Ll1ch"
                   },
                   {
                       "title": "Waking Up In Vegas",
                       "artist": "Katy Perry",
                       "is_new": false,
                       "last_pos": 65,
                       "peak_pos": 51,
                       "rank": 51,
                       "weeks": 2,
                       "_id": "6fvxos1qSHrIgOkKw4dhWS"
                   },
                   {
                       "title": "All The Above",
                       "artist": "Maino Featuring T-Pain",
                       "is_new": false,
                       "last_pos": 47,
                       "peak_pos": 39,
                       "rank": 52,
                       "weeks": 11
                   },
                   {
                       "title": "Kiss A Girl",
                       "artist": "Keith Urban",
                       "is_new": false,
                       "last_pos": 57,
                       "peak_pos": 51,
                       "rank": 53,
                       "weeks": 9,
                       "_id": "4DyKZmONpoggnn3piyjad2"
                   },
                   {
                       "title": "I Love College",
                       "artist": "Asher Roth",
                       "is_new": false,
                       "last_pos": 39,
                       "peak_pos": 12,
                       "rank": 54,
                       "weeks": 13,
                       "_id": "1akgiRM3mN2nxu2AX6ACCW"
                   },
                   {
                       "title": "Use Somebody",
                       "artist": "Kings Of Leon",
                       "is_new": false,
                       "last_pos": 55,
                       "peak_pos": 53,
                       "rank": 55,
                       "weeks": 15,
                       "_id": "5VGlqQANWDKJFl0MBG3sg2"
                   },
                   {
                       "title": "One In Every Crowd",
                       "artist": "Montgomery Gentry",
                       "is_new": false,
                       "last_pos": 58,
                       "peak_pos": 56,
                       "rank": 56,
                       "weeks": 9,
                       "_id": "6HAsk04nz2TaEbK8lncN3w"
                   },
                   {
                       "title": "Best Days Of Your Life",
                       "artist": "Kellie Pickler",
                       "is_new": false,
                       "last_pos": 54,
                       "peak_pos": 48,
                       "rank": 57,
                       "weeks": 5,
                       "_id": "27BMLvWD1TYdosiReVgbAt"
                   },
                   {
                       "title": "Hoedown Throwdown",
                       "artist": "Miley Cyrus",
                       "is_new": false,
                       "last_pos": 45,
                       "peak_pos": 18,
                       "rank": 58,
                       "weeks": 9,
                       "_id": "6ntkwU1MhehTKgx4BWxX3f"
                   },
                   {
                       "title": "Lucky",
                       "artist": "Jason Mraz & Colbie Caillat",
                       "is_new": false,
                       "last_pos": 59,
                       "peak_pos": 48,
                       "rank": 59,
                       "weeks": 18,
                       "_id": "0qhirpH8RdiUW6aphRi7UJ"
                   },
                   {
                       "title": "Welcome To The World",
                       "artist": "Kevin Rudolf Featuring Rick Ross",
                       "is_new": false,
                       "last_pos": 60,
                       "peak_pos": 60,
                       "rank": 60,
                       "weeks": 4
                   },
                   {
                       "title": "LoveGame",
                       "artist": "Lady Gaga",
                       "is_new": false,
                       "last_pos": 81,
                       "peak_pos": 61,
                       "rank": 61,
                       "weeks": 4,
                       "_id": "0TcJ7QWpggdSg8t0fHThHm"
                   },
                   {
                       "title": "It's America",
                       "artist": "Rodney Atkins",
                       "is_new": false,
                       "last_pos": 53,
                       "peak_pos": 44,
                       "rank": 62,
                       "weeks": 12,
                       "_id": "5zGqTc1WQUe8XqqdnrpKTV"
                   },
                   {
                       "title": "That's Not My Name",
                       "artist": "The Ting Tings",
                       "is_new": false,
                       "last_pos": 56,
                       "peak_pos": 52,
                       "rank": 63,
                       "weeks": 18,
                       "_id": "2DNdEpV9UnsYjL6w1Dp1aS"
                   },
                   {
                       "title": "Not Meant To Be",
                       "artist": "Theory Of A Deadman",
                       "is_new": false,
                       "last_pos": 63,
                       "peak_pos": 63,
                       "rank": 64,
                       "weeks": 5,
                       "_id": "5xyACR2lzIyzFepF4qlAas"
                   },
                   {
                       "title": "Careless Whisper",
                       "artist": "Seether",
                       "is_new": false,
                       "last_pos": 64,
                       "peak_pos": 64,
                       "rank": 65,
                       "weeks": 10,
                       "_id": "46eHxWRKmtYhUHPDRgclt6"
                   },
                   {
                       "title": "Crazier",
                       "artist": "Taylor Swift",
                       "is_new": false,
                       "last_pos": 61,
                       "peak_pos": 17,
                       "rank": 66,
                       "weeks": 7,
                       "_id": "5vyxXfD5gLlyPxGZMEjtmd"
                   },
                   {
                       "title": "Never Ever",
                       "artist": "Ciara Featuring Young Jeezy",
                       "is_new": false,
                       "last_pos": 66,
                       "peak_pos": 66,
                       "rank": 67,
                       "weeks": 12
                   },
                   {
                       "title": "Sissy's Song",
                       "artist": "Alan Jackson",
                       "is_new": false,
                       "last_pos": 71,
                       "peak_pos": 68,
                       "rank": 68,
                       "weeks": 5,
                       "_id": "68qJGt0E1gB4MaQOZcGGK5"
                   },
                   {
                       "title": "Show Me What I'm Looking For",
                       "artist": "Carolina Liar",
                       "is_new": false,
                       "last_pos": 80,
                       "peak_pos": 69,
                       "rank": 69,
                       "weeks": 4,
                       "_id": "2h8iXIwEdEnQywhFC4q5e5"
                   },
                   {
                       "title": "Ain't I",
                       "artist": "Yung L.A. Featuring Young Dro & T.I.",
                       "is_new": false,
                       "last_pos": 62,
                       "peak_pos": 47,
                       "rank": 70,
                       "weeks": 15
                   },
                   {
                       "title": "Always The Love Songs",
                       "artist": "Eli Young Band",
                       "is_new": false,
                       "last_pos": 79,
                       "peak_pos": 71,
                       "rank": 71,
                       "weeks": 8,
                       "_id": "3RFwzA2lXqIzoXHVbeLH1d"
                   },
                   {
                       "title": "I'm On A Boat",
                       "artist": "The Lonely Island Featuring T-Pain",
                       "is_new": false,
                       "last_pos": 72,
                       "peak_pos": 65,
                       "rank": 72,
                       "weeks": 13,
                       "_id": "7wwhDmpjiy1rzJ3zWy5zur"
                   },
                   {
                       "title": "Where I'm From",
                       "artist": "Jason Michael Carroll",
                       "is_new": false,
                       "last_pos": 74,
                       "peak_pos": 73,
                       "rank": 73,
                       "weeks": 5,
                       "_id": "0VeI4Zyirc6yuJ4P7fudjE"
                   },
                   {
                       "title": "Out Last Night",
                       "artist": "Kenny Chesney",
                       "is_new": false,
                       "last_pos": 77,
                       "peak_pos": 74,
                       "rank": 74,
                       "weeks": 4,
                       "_id": "4QoZGQ8woaho94v629kVl1"
                   },
                   {
                       "title": "Magnificent",
                       "artist": "Rick Ross Featuring John Legend",
                       "is_new": false,
                       "last_pos": 67,
                       "peak_pos": 62,
                       "rank": 75,
                       "weeks": 10
                   },
                   {
                       "title": "If This Isn't Love",
                       "artist": "Jennifer Hudson",
                       "is_new": false,
                       "last_pos": 75,
                       "peak_pos": 63,
                       "rank": 76,
                       "weeks": 11,
                       "_id": "0o6IWuQ9Uxt69jKdVZ1eg2"
                   },
                   {
                       "title": "Stanky Legg",
                       "artist": "GS Boyz",
                       "is_new": false,
                       "last_pos": 69,
                       "peak_pos": 49,
                       "rank": 77,
                       "weeks": 14,
                       "_id": "73GD9DI2EBRSi2l57wyh3Z"
                   },
                   {
                       "title": "Echo",
                       "artist": "Gorilla Zoe",
                       "is_new": false,
                       "last_pos": 82,
                       "peak_pos": 78,
                       "rank": 78,
                       "weeks": 2,
                       "_id": "7ytLGF9Ua5g5MylbwtxzZC"
                   },
                   {
                       "title": "Beggin'",
                       "artist": "Madcon",
                       "is_new": false,
                       "last_pos": 84,
                       "peak_pos": 79,
                       "rank": 79,
                       "weeks": 4,
                       "_id": "4YeKAwFn69ehnciOfi7Sbf"
                   },
                   {
                       "title": "It Won't Be Like This For Long",
                       "artist": "Darius Rucker",
                       "is_new": false,
                       "last_pos": 68,
                       "peak_pos": 36,
                       "rank": 80,
                       "weeks": 19,
                       "_id": "4gzeYkzuzxuzAUTsGcdjqA"
                   },
                   {
                       "title": "Amazing",
                       "artist": "Kanye West Featuring Young Jeezy",
                       "is_new": false,
                       "last_pos": 0,
                       "peak_pos": 81,
                       "rank": 81,
                       "weeks": 2,
                       "_id": "3HsFsVAcvIx4nThFUUHHSK"
                   },
                   {
                       "title": "People Are Crazy",
                       "artist": "Billy Currington",
                       "is_new": false,
                       "last_pos": 90,
                       "peak_pos": 82,
                       "rank": 82,
                       "weeks": 2,
                       "_id": "5JnLiuMgRAWoHYOhatFP3s"
                   },
                   {
                       "title": "Halle Berry (She's Fine)",
                       "artist": "Hurricane Chris Featuring SupaSTAAR",
                       "is_new": false,
                       "last_pos": 91,
                       "peak_pos": 83,
                       "rank": 83,
                       "weeks": 2
                   },
                   {
                       "title": "Pretty Wings",
                       "artist": "Maxwell",
                       "is_new": false,
                       "last_pos": 89,
                       "peak_pos": 84,
                       "rank": 84,
                       "weeks": 2,
                       "_id": "22NLm3IIR9NLG0cUYtmHMW"
                   },
                   {
                       "title": "You Can Get It All",
                       "artist": "Bow Wow Featuring Johnta Austin",
                       "is_new": false,
                       "last_pos": 70,
                       "peak_pos": 55,
                       "rank": 85,
                       "weeks": 10
                   },
                   {
                       "title": "Crack A Bottle",
                       "artist": "Eminem, Dr. Dre & 50 Cent",
                       "is_new": false,
                       "last_pos": 76,
                       "peak_pos": 1,
                       "rank": 86,
                       "weeks": 17,
                       "_id": "7oVOivJpAkDmpP4DMkCE29"
                   },
                   {
                       "title": "I'm Just Here For The Music",
                       "artist": "Paula Abdul",
                       "is_new": true,
                       "last_pos": 0,
                       "peak_pos": 87,
                       "rank": 87,
                       "weeks": 1,
                       "_id": "0B8yh3jfMdQM4Xtfidm2RC"
                   },
                   {
                       "title": "Jai Ho! (You Are My Destiny)",
                       "artist": "A R Rahman & The Pussycat Dolls Featuring Nicole Scherzinger",
                       "is_new": false,
                       "last_pos": 85,
                       "peak_pos": 15,
                       "rank": 88,
                       "weeks": 12,
                       "_id": "42nuEyGSHayNMoi5Qpt6o8"
                   },
                   {
                       "title": "Epiphany (I'm Leaving)",
                       "artist": "Chrisette Michele",
                       "is_new": false,
                       "last_pos": 92,
                       "peak_pos": 89,
                       "rank": 89,
                       "weeks": 2,
                       "_id": "3oQ5kA2PGJlztciJk3SKA0"
                   },
                   {
                       "title": "Funny The Way It Is",
                       "artist": "Dave Matthews Band",
                       "is_new": false,
                       "last_pos": 73,
                       "peak_pos": 37,
                       "rank": 90,
                       "weeks": 3,
                       "_id": "4ZOoV2Ofzm5gnM3rVl9q7d"
                   },
                   {
                       "title": "sobeautiful",
                       "artist": "Musiq Soulchild",
                       "is_new": false,
                       "last_pos": 94,
                       "peak_pos": 89,
                       "rank": 91,
                       "weeks": 8,
                       "_id": "2PN3gbuBn5WBEwrEJH3xiu"
                   },
                   {
                       "title": "Best I Ever Had",
                       "artist": "Drake",
                       "is_new": true,
                       "last_pos": 0,
                       "peak_pos": 92,
                       "rank": 92,
                       "weeks": 1,
                       "_id": "3QLjDkgLh9AOEHlhQtDuhs"
                   },
                   {
                       "title": "Lost You Anyway",
                       "artist": "Toby Keith",
                       "is_new": false,
                       "last_pos": 97,
                       "peak_pos": 93,
                       "rank": 93,
                       "weeks": 2,
                       "_id": "25KX7nIM4INAkiGH7LY8xF"
                   },
                   {
                       "title": "Always Strapped",
                       "artist": "Birdman Featuring Lil Wayne",
                       "is_new": false,
                       "last_pos": 93,
                       "peak_pos": 65,
                       "rank": 94,
                       "weeks": 5
                   },
                   {
                       "title": "Don't Think I Can't Love You",
                       "artist": "Jake Owen",
                       "is_new": false,
                       "last_pos": 88,
                       "peak_pos": 57,
                       "rank": 95,
                       "weeks": 15,
                       "_id": "1SoP2zC2ttcI2zjuRsQ5xx"
                   },
                   {
                       "title": "Every Girl",
                       "artist": "Young Money",
                       "is_new": true,
                       "last_pos": 0,
                       "peak_pos": 96,
                       "rank": 96,
                       "weeks": 1,
                       "_id": "6klZDCEsQzMELAPyovvpUw"
                   },
                   {
                       "title": "Don't Forget",
                       "artist": "Demi Lovato",
                       "is_new": false,
                       "last_pos": 83,
                       "peak_pos": 41,
                       "rank": 97,
                       "weeks": 9,
                       "_id": "4He7bzKBaiu7CRz4wGkZzx"
                   },
                   {
                       "title": "Next To You",
                       "artist": "Mike Jones",
                       "is_new": false,
                       "last_pos": 86,
                       "peak_pos": 71,
                       "rank": 98,
                       "weeks": 10,
                       "_id": "7xVwhsuZTUWhIyFeDH6rYr"
                   },
                   {
                       "title": "Swag Surfin'",
                       "artist": "F.L.Y. (Fast Life Yungstaz)",
                       "is_new": true,
                       "last_pos": 0,
                       "peak_pos": 99,
                       "rank": 99,
                       "weeks": 1,
                       "_id": "5ItzU5pBrFmRUudfr5RkJP"
                   },
                   {
                       "title": "There Goes My Baby",
                       "artist": "Charlie Wilson",
                       "is_new": false,
                       "last_pos": 98,
                       "peak_pos": 98,
                       "rank": 100,
                       "weeks": 2,
                       "_id": "6rZsGpXDr0WsUSFitx3v8N"
                   }
               ]
           }''')
    album = json.loads('''{
           "title": "Lemonade",
           "_id": "7dK54iZuOxXFarGhXwEXfF",
           "release_date": "2016-04-23",
           "images": [
               {
                   "height": 640,
                   "url": "https://i.scdn.co/image/ab67616d0000b27389992f4d7d4ab94937bf9e23",
                   "width": 640
               },
               {
                   "height": 300,
                   "url": "https://i.scdn.co/image/ab67616d00001e0289992f4d7d4ab94937bf9e23",
                   "width": 300
               },
               {
                   "height": 64,
                   "url": "https://i.scdn.co/image/ab67616d0000485189992f4d7d4ab94937bf9e23",
                   "width": 64
               }
           ],
           "artists": [
               {
                   "name": "Beyonc\u00e9",
                   "_id": "6vWDO969PvNqNYHIOW5v0m"
               }
           ],
           "tracks": [
               {
                   "title": "Pray You Catch Me",
                   "_id": "7rLDARtJALM7QdiJDMXW7m",
                   "duration_ms": 195986,
                   "preview_url": "https://p.scdn.co/mp3-preview/88351cf8f3e5023b03b9ae0add806ad44288f22b?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 1,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               },
               {
                   "title": "Hold Up",
                   "_id": "0rzNMzZsubFcXSEh7dnem7",
                   "duration_ms": 221093,
                   "preview_url": "https://p.scdn.co/mp3-preview/45ca175a1ceffc733b2d62dfc49e6ede386415c1?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 2,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               },
               {
                   "title": "Don't Hurt Yourself (feat. Jack White)",
                   "_id": "78eouBKVRyhbSzJwChr6QM",
                   "duration_ms": 233666,
                   "preview_url": "https://p.scdn.co/mp3-preview/ca8aa1d17b603b92587e4e850979b19ea5d61a3b?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 3,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       },
                       {
                           "name": "Jack White",
                           "_id": "4FZ3j1oH43e7cukCALsCwf"
                       }
                   ]
               },
               {
                   "title": "Sorry",
                   "_id": "0lnIJmgcUpEpe4AZACjayW",
                   "duration_ms": 232560,
                   "preview_url": "https://p.scdn.co/mp3-preview/5f4f6414a73c124d629ffe0b0bc8aa679ef10a7c?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 4,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               },
               {
                   "title": "6 Inch (feat. The Weeknd)",
                   "_id": "0pxNyJJiL2zZZ5GgJ4JhsQ",
                   "duration_ms": 260440,
                   "preview_url": "https://p.scdn.co/mp3-preview/4c37b4f1134701ab01d4b7159137adeab8e5fdad?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 5,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       },
                       {
                           "name": "The Weeknd",
                           "_id": "1Xyo4u8uXC1ZmMpatF05PJ"
                       }
                   ]
               },
               {
                   "title": "Daddy Lessons",
                   "_id": "71OvX5NNLrmz7rpq1ANTQn",
                   "duration_ms": 287986,
                   "preview_url": "https://p.scdn.co/mp3-preview/aa63f0afae4514f698085d43fe30f3e237e9f8b7?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 6,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               },
               {
                   "title": "Love Drought",
                   "_id": "2RQAG0wQt35UzAPEyVJFWN",
                   "duration_ms": 237440,
                   "preview_url": "https://p.scdn.co/mp3-preview/b06ef3adf5b871065002328b3949e0ca341057fd?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 7,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               },
               {
                   "title": "Sandcastles",
                   "_id": "2FNPJWVdGxYhWaotd6rULS",
                   "duration_ms": 182720,
                   "preview_url": "https://p.scdn.co/mp3-preview/4bbf704b5d7731153324eda2210955084e0214c1?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 8,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               },
               {
                   "title": "Forward (feat. James Blake)",
                   "_id": "2bOWJIYSbhwg2NMQq1QMYH",
                   "duration_ms": 79200,
                   "preview_url": "https://p.scdn.co/mp3-preview/cbe71084f4cd278c646595e620f90dd6504adacc?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 9,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       },
                       {
                           "name": "James Blake",
                           "_id": "53KwLdlmrlCelAZMaLVZqU"
                       }
                   ]
               },
               {
                   "title": "Freedom (feat. Kendrick Lamar)",
                   "_id": "7aBxcRw77817BrkdPChAGY",
                   "duration_ms": 289760,
                   "preview_url": "https://p.scdn.co/mp3-preview/fbe70f454ebeab5073a4db690b1c1bbfcd396811?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 10,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       },
                       {
                           "name": "Kendrick Lamar",
                           "_id": "2YZyLoL8N0Wb9xBt1NhZWg"
                       }
                   ]
               },
               {
                   "title": "All Night",
                   "_id": "7oAuqs6akGnPU3Tb00ZmyM",
                   "duration_ms": 322000,
                   "preview_url": "https://p.scdn.co/mp3-preview/60672180e28c45d7ac6f6b64f444dcfcaa5f9548?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 11,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               },
               {
                   "title": "Formation",
                   "_id": "6g0Orsxv6glTJCt4cHsRsQ",
                   "duration_ms": 206080,
                   "preview_url": "https://p.scdn.co/mp3-preview/470cfb59470d8313dc99634c2845170b56a8762e?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 12,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               },
               {
                   "title": "Sorry - Original Demo",
                   "_id": "6eR1N0EBHiDkGDzegX99d3",
                   "duration_ms": 204400,
                   "preview_url": "https://p.scdn.co/mp3-preview/3fd961d27feebd6f6aea3f7572f1a52532c959b7?cid=9c6784a0ec4d4cb7a131d7192891e3f9",
                   "track_number": 13,
                   "album": {
                       "name": "Lemonade",
                       "_id": "7dK54iZuOxXFarGhXwEXfF"
                   },
                   "artists": [
                       {
                           "name": "Beyonc\u00e9",
                           "_id": "6vWDO969PvNqNYHIOW5v0m"
                       }
                   ]
               }
           ]
       }''')

    artist = json.loads(
        r'''{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m", "images": [{"height": 640, "url": "https://i.scdn.co/image/ad8b0e5a18a5a443a2678768bd73f59833941abc", "width": 640}, {"height": 320, "url": "https://i.scdn.co/image/802895be7bc5339087ba36194b0b7307c467df96", "width": 320}, {"height": 160, "url": "https://i.scdn.co/image/a932ba0a31bd2807fe76c77b64c680bec2f3d14a", "width": 160}], "genres": ["dance pop", "pop", "pop dance", "post-teen pop", "r&b"], "followers": 26744700}''')
    track = json.loads(
        r'''{"title": "Crazy In Love (feat. Jay-Z)", "_id": "0TwBtDAWpkpM3srywFVOV5", "duration_ms": 235933, "preview_url": "https://p.scdn.co/mp3-preview/101049761a9938ee570cf2825eea3521de25d5d0?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "Dangerously In Love", "_id": "06v9eHnqhMK2tbM2Iz3p0Y"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "JAY-Z", "_id": "3nFkdlSjzX9mRTtwJOzDYB"}], "audio_features": {"danceability": 0.664, "energy": 0.758, "key": 2, "loudness": -6.583, "mode": 0, "speechiness": 0.21, "acousticness": 0.00238, "instrumentalness": 0, "liveness": 0.0598, "valence": 0.701, "tempo": 99.259, "duration_ms": 235933, "time_signature": 4}}''')
    playlist = json.loads(
        r'''{"name": "This Is Beyonc\u00e9", "_id": "37i9dQZF1DX2oU49YwtXI2", "followers": 1262173, "tracks": [{"title": "BLACK PARADE", "_id": "2qzUpSVI4NnPyWxbXwumTj", "duration_ms": 281272, "preview_url": "https://p.scdn.co/mp3-preview/b2e3679ec741b6b530892ba641752e82d3fcc9aa?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "BLACK PARADE", "_id": "3MJxH055n52Rbm8RLlpJcN"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "SPIRIT - From Disney's \"The Lion King\"", "_id": "6ucmgsair6pvRYfwgvI6e0", "duration_ms": 277030, "preview_url": "https://p.scdn.co/mp3-preview/0a3e900fb7ef798fa4cec851abdd957f1d82c645?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 27, "album": {"name": "The Lion King: The Gift", "_id": "552zi1M53PQAX5OH4FIdTx"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "FIND YOUR WAY BACK - MELO-X Remix", "_id": "1g0k1bFcLKyIrdsdUkF3eW", "duration_ms": 216339, "preview_url": "https://p.scdn.co/mp3-preview/347e3e79cb1d939859f7da6a869a02198b7d007d?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 16, "album": {"name": "The Lion King: The Gift [Deluxe Edition]", "_id": "7kUuNU2LRmr9XbwLHXU9UZ"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "Melo-X", "_id": "6AxHPycFTjxDROqK1pzsAE"}]}, {"title": "Halo", "_id": "4JehYebiI9JE8sR8MisGVb", "duration_ms": 261640, "preview_url": "https://p.scdn.co/mp3-preview/3c97985f3736fab6d4abcd2067f346a9b30955fa?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "I AM...SASHA FIERCE", "_id": "39P7VD7qlg3Z0ltq60eHp7"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Love On Top", "_id": "1z6WtY7X4HQJvzxC4UgkSf", "duration_ms": 267413, "preview_url": "https://p.scdn.co/mp3-preview/9a7675cc7f71c524af1397cf1675b61a8fbc74ed?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "4", "_id": "1gIC63gC3B7o7FfpPACZQJ"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Formation", "_id": "6g0Orsxv6glTJCt4cHsRsQ", "duration_ms": 206080, "preview_url": "https://p.scdn.co/mp3-preview/470cfb59470d8313dc99634c2845170b56a8762e?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 12, "album": {"name": "Lemonade", "_id": "7dK54iZuOxXFarGhXwEXfF"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Crazy In Love (feat. Jay-Z)", "_id": "5IVuqXILoxVWvWEPm82Jxr", "duration_ms": 236133, "preview_url": "https://p.scdn.co/mp3-preview/ce8ace0ec425840416be78db07cf50dd331eed4f?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "Dangerously In Love", "_id": "6oxVabMIqCMJRYN1GqR3Vf"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "JAY-Z", "_id": "3nFkdlSjzX9mRTtwJOzDYB"}]}, {"title": "If I Were a Boy", "_id": "1oQZk2bKBLgP1cbuFKvjkq", "duration_ms": 249866, "preview_url": "https://p.scdn.co/mp3-preview/bfd4494effb973d223be6d0c3378d7cfc60e15ca?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 5, "album": {"name": "I AM...SASHA FIERCE - Platinum Edition", "_id": "3ROfBX6lJLnCmaw1NrP5K9"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "BROWN SKIN GIRL", "_id": "0B3FovCVaGKS5w1FTidEUP", "duration_ms": 248472, "preview_url": "https://p.scdn.co/mp3-preview/919c83819c5bb9a0097635dd978f9c74135a9cc2?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 15, "album": {"name": "The Lion King: The Gift", "_id": "552zi1M53PQAX5OH4FIdTx"}, "artists": [{"name": "Blue Ivy", "_id": "3XV0lFzNs8BCTwhJTcMiBr"}, {"name": "SAINt JHN", "_id": "0H39MdGGX6dbnnQPt6NQkZ"}, {"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "WizKid", "_id": "3tVQdUvClmAT7URs9V3rsp"}]}, {"title": "Jumpin', Jumpin'", "_id": "4pmc2AxSEq6g7hPVlJCPyP", "duration_ms": 230200, "preview_url": "https://p.scdn.co/mp3-preview/900921fe7ea36b001edc1c21f146c4732ff43032?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 11, "album": {"name": "The Writing's On The Wall", "_id": "283NWqNsCA9GwVHrJk59CG"}, "artists": [{"name": "Destiny's Child", "_id": "1Y8cdNmUJH7yBTd9yOvr5i"}]}, {"title": "Hold Up", "_id": "0rzNMzZsubFcXSEh7dnem7", "duration_ms": 221093, "preview_url": "https://p.scdn.co/mp3-preview/45ca175a1ceffc733b2d62dfc49e6ede386415c1?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "Lemonade", "_id": "7dK54iZuOxXFarGhXwEXfF"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Partition", "_id": "2vPTtiR7x7T6Lr17CE2FAE", "duration_ms": 319466, "preview_url": "https://p.scdn.co/mp3-preview/8ecd5010e5a46f1c84f42bcfed50505395825d7a?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "Partition", "_id": "1hq4Vrcbua3DDBLhuWFEVQ"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Telephone", "_id": "1IaYWv32nFFMdljBIjMY5T", "duration_ms": 220626, "preview_url": null, "track_number": 6, "album": {"name": "The Fame Monster", "_id": "2bsK9FeKzU0jNKA6XYFP7E"}, "artists": [{"name": "Lady Gaga", "_id": "1HY2Jd0NmPuamShAr6KMms"}, {"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Run the World (Girls)", "_id": "1uXbwHHfgsXcUKfSZw5ZJ0", "duration_ms": 236093, "preview_url": "https://p.scdn.co/mp3-preview/c5e90d8d93467fc4aeda29c2cbefaa494081232a?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 11, "album": {"name": "4", "_id": "1gIC63gC3B7o7FfpPACZQJ"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Irreplaceable", "_id": "6RX5iL93VZ5fKmyvNXvF1r", "duration_ms": 227853, "preview_url": "https://p.scdn.co/mp3-preview/b1cd9ecf3b5108363846f497897b5bcbec44343a?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "B'Day Deluxe Edition", "_id": "0Zd10MKN5j9KwUST0TdBBB"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Single Ladies (Put a Ring on It)", "_id": "2ZBNclC5wm4GtiWaeh0DMx", "duration_ms": 193213, "preview_url": "https://p.scdn.co/mp3-preview/69c8bae97df9121ee440b23053fe20f64c204408?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "I AM...SASHA FIERCE", "_id": "23Y5wdyP5byMFktZf8AcWU"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Sorry", "_id": "0lnIJmgcUpEpe4AZACjayW", "duration_ms": 232560, "preview_url": "https://p.scdn.co/mp3-preview/5f4f6414a73c124d629ffe0b0bc8aa679ef10a7c?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 4, "album": {"name": "Lemonade", "_id": "7dK54iZuOxXFarGhXwEXfF"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Best Thing I Never Had", "_id": "3lBRNqXjPp2j3JMTCXDTNO", "duration_ms": 253746, "preview_url": "https://p.scdn.co/mp3-preview/86466c6e8a9c56977255b4ed91f22a335ed5467e?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 12, "album": {"name": "4", "_id": "1gIC63gC3B7o7FfpPACZQJ"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Feeling Myself", "_id": "5fyIGoaaKelzdyW8ELhYJZ", "duration_ms": 237840, "preview_url": null, "track_number": 5, "album": {"name": "The Pinkprint (Deluxe)", "_id": "5ooCuPIk58IwSo6DRr1JCu"}, "artists": [{"name": "Nicki Minaj", "_id": "0hCNtLu0JehylgoiP8L4Gh"}, {"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Drunk in Love (feat. Jay-Z)", "_id": "6jG2YzhxptolDzLHTGLt7S", "duration_ms": 323480, "preview_url": "https://p.scdn.co/mp3-preview/7052c45ae9298c38fdbd1b45bec7fc5f554fafa0?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 3, "album": {"name": "BEYONC\u00c9 [Platinum Edition]", "_id": "2UJwKSBUz6rtW4QLK74kQu"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "JAY-Z", "_id": "3nFkdlSjzX9mRTtwJOzDYB"}]}, {"title": "Daddy Lessons", "_id": "71OvX5NNLrmz7rpq1ANTQn", "duration_ms": 287986, "preview_url": "https://p.scdn.co/mp3-preview/aa63f0afae4514f698085d43fe30f3e237e9f8b7?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 6, "album": {"name": "Lemonade", "_id": "7dK54iZuOxXFarGhXwEXfF"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "SPIRIT - From Disney's \"The Lion King\"", "_id": "1tC5WMSlsGDFfqnrquB1ty", "duration_ms": 277030, "preview_url": "https://p.scdn.co/mp3-preview/6b941ab322b1695adb56be853465ab922303196f?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "SPIRIT (From Disney's \"The Lion King\")", "_id": "6W0hMM8vjhNhDOjfiORZE4"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "APESHIT", "_id": "62GXGpd73vslqIBHq8XqOx", "duration_ms": 264853, "preview_url": "https://p.scdn.co/mp3-preview/ab8b0397e06448340bdaee986669ba3ccb875017?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "EVERYTHING IS LOVE", "_id": "7jbdod8XNRfe2nIhppht46"}, "artists": [{"name": "The Carters", "_id": "4fpTMHe34LC5t3h5ztK8qu"}]}, {"title": "7/11", "_id": "02M6vucOvmRfMxTXDUwRXu", "duration_ms": 213506, "preview_url": "https://p.scdn.co/mp3-preview/c17eaf47f9624029dadcd7672b7896c84923e28c?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "BEYONC\u00c9 [Platinum Edition]", "_id": "2UJwKSBUz6rtW4QLK74kQu"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Mi Gente (feat. Beyonc\u00e9)", "_id": "7fwXWKdDNI5IutOMc5OKYw", "duration_ms": 209733, "preview_url": null, "track_number": 1, "album": {"name": "Mi Gente (feat. Beyonc\u00e9)", "_id": "0ARVq1kA5eRP4F5VsZsr3m"}, "artists": [{"name": "J Balvin", "_id": "1vyhD5VmyZ7KMfW5gqLgo5"}, {"name": "Willy William", "_id": "4RSyJzf7ef6Iu2rnLdabNq"}, {"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Girl", "_id": "3s2MyU2YCwNNwcSokt0jXD", "duration_ms": 224146, "preview_url": "https://p.scdn.co/mp3-preview/686c0ba0056aed5e9b50f3fa05c1e8a5674b4859?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 6, "album": {"name": "Destiny Fulfilled", "_id": "0b6ivSFfDs38MG7aLn9rvO"}, "artists": [{"name": "Destiny's Child", "_id": "1Y8cdNmUJH7yBTd9yOvr5i"}]}, {"title": "Independent Women, Pt. 1", "_id": "69XUpOpjzDKcfdxqZebGiI", "duration_ms": 221133, "preview_url": "https://p.scdn.co/mp3-preview/ec1b786b6675b03448e22f4538d244151512db68?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "Survivor", "_id": "2HcjLD0ButtKsQYqzoyOx9"}, "artists": [{"name": "Destiny's Child", "_id": "1Y8cdNmUJH7yBTd9yOvr5i"}]}, {"title": "XO", "_id": "04cxAqa9ZgLwvEskosNVsB", "duration_ms": 215946, "preview_url": "https://p.scdn.co/mp3-preview/df9b5efc4577e9ef65491b5a3f74f3cf428297d3?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "BEYONC\u00c9", "_id": "2noKUZhXwUhPQMgSr56T4G"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Bootylicious", "_id": "41nT1Sp6ChR65FbsdLlFHW", "duration_ms": 207906, "preview_url": "https://p.scdn.co/mp3-preview/744f405691f6a13bc60ebce53b10c0cf68d8ded2?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 3, "album": {"name": "Survivor", "_id": "2HcjLD0ButtKsQYqzoyOx9"}, "artists": [{"name": "Destiny's Child", "_id": "1Y8cdNmUJH7yBTd9yOvr5i"}]}, {"title": "Lose My Breath - Homecoming Live", "_id": "716IzgeD9yjfCiZuG9aG4i", "duration_ms": 90815, "preview_url": "https://p.scdn.co/mp3-preview/496552a6efefd9faa29567cadc7a38bf6266658e?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 31, "album": {"name": "HOMECOMING: THE LIVE ALBUM", "_id": "35S1JCj5paIfElT2GODl6x"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "Kelly Rowland", "_id": "3AuMNF8rQAKOzjYppFNAoB"}, {"name": "Michelle Williams", "_id": "6t7nbFAc2dUa7oNu7kBOui"}]}, {"title": "Baby Boy (feat. Sean Paul)", "_id": "4WY3HyGXsWqjFRCVD6gnTe", "duration_ms": 244826, "preview_url": "https://p.scdn.co/mp3-preview/589c1d95875fad5e369bb9cc2e52b938de6537a2?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 3, "album": {"name": "Dangerously In Love", "_id": "6oxVabMIqCMJRYN1GqR3Vf"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "Sean Paul", "_id": "3Isy6kedDrgPYoTS1dazA9"}]}, {"title": "All Night", "_id": "7oAuqs6akGnPU3Tb00ZmyM", "duration_ms": 322000, "preview_url": "https://p.scdn.co/mp3-preview/60672180e28c45d7ac6f6b64f444dcfcaa5f9548?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 11, "album": {"name": "Lemonade", "_id": "7dK54iZuOxXFarGhXwEXfF"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Say My Name", "_id": "7H6ev70Weq6DdpZyyTmUXk", "duration_ms": 271333, "preview_url": "https://p.scdn.co/mp3-preview/26cde6756e631b05cb8b83e99bec8eac00980073?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 12, "album": {"name": "The Writing's On The Wall", "_id": "283NWqNsCA9GwVHrJk59CG"}, "artists": [{"name": "Destiny's Child", "_id": "1Y8cdNmUJH7yBTd9yOvr5i"}]}, {"title": "Survivor", "_id": "2Mpj1Ul5OFPyyP4wB62Rvi", "duration_ms": 254026, "preview_url": "https://p.scdn.co/mp3-preview/c48860e5c7ce2f6a312b6bc4efd16de85ec49216?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "Survivor", "_id": "2HcjLD0ButtKsQYqzoyOx9"}, "artists": [{"name": "Destiny's Child", "_id": "1Y8cdNmUJH7yBTd9yOvr5i"}]}, {"title": "Sweet Dreams", "_id": "2wNGoon7FlKnVEyYS1ZRBQ", "duration_ms": 207480, "preview_url": "https://p.scdn.co/mp3-preview/209c7988f8480962725ee394d4242e869769832b?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 7, "album": {"name": "I AM...SASHA FIERCE - Platinum Edition", "_id": "3ROfBX6lJLnCmaw1NrP5K9"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Flawless Remix (feat. Nicki Minaj)", "_id": "0zVMzJ37VQNFUNvdxxat2E", "duration_ms": 234413, "preview_url": "https://p.scdn.co/mp3-preview/89a75c3b0d197bc20f28ae18f586971638a4df6d?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "BEYONC\u00c9 [Platinum Edition]", "_id": "2UJwKSBUz6rtW4QLK74kQu"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "Nicki Minaj", "_id": "0hCNtLu0JehylgoiP8L4Gh"}]}, {"title": "Broken-Hearted Girl", "_id": "7zP67rufQgoODWFI45jntD", "duration_ms": 278400, "preview_url": "https://p.scdn.co/mp3-preview/77f13254c0f6b19c6639a66c9e9cb1451addd6f1?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 8, "album": {"name": "I AM...SASHA FIERCE - Platinum Edition", "_id": "3ROfBX6lJLnCmaw1NrP5K9"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Soldier (feat. T.I. & Lil' Wayne)", "_id": "4FTOpNYcGxnQdGNWSxIcio", "duration_ms": 325573, "preview_url": "https://p.scdn.co/mp3-preview/dd12122fbfe9cebcffc7e4f30de061508d3b5ca4?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "Destiny Fulfilled", "_id": "0b6ivSFfDs38MG7aLn9rvO"}, "artists": [{"name": "Destiny's Child", "_id": "1Y8cdNmUJH7yBTd9yOvr5i"}, {"name": "T.I.", "_id": "4OBJLual30L7gRl5UkeRcT"}, {"name": "Lil Wayne", "_id": "55Aa2cqylxrFIXC767Z865"}]}, {"title": "Me, Myself and I", "_id": "3pxJuMLjNPtiC0fX8EHFlF", "duration_ms": 301173, "preview_url": "https://p.scdn.co/mp3-preview/8383f4770d57a2bd8ce507b0ba059e3bbb6bdd79?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 6, "album": {"name": "Dangerously In Love", "_id": "06v9eHnqhMK2tbM2Iz3p0Y"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "03' Bonnie & Clyde", "_id": "5ljCWsDlSyJ41kwqym2ORw", "duration_ms": 205560, "preview_url": null, "track_number": 4, "album": {"name": "The Blueprint 2 The Gift & The Curse", "_id": "5xHStEOG8PsbzNQb7LkxZU"}, "artists": [{"name": "JAY-Z", "_id": "3nFkdlSjzX9mRTtwJOzDYB"}, {"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Bills, Bills, Bills", "_id": "1Oi2zpmL81Q0yScF1zxaC0", "duration_ms": 256026, "preview_url": "https://p.scdn.co/mp3-preview/5947a78d5f8dbf6cd776b9fef4aedda4c406f8ba?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 3, "album": {"name": "The Writing's On The Wall", "_id": "283NWqNsCA9GwVHrJk59CG"}, "artists": [{"name": "Destiny's Child", "_id": "1Y8cdNmUJH7yBTd9yOvr5i"}]}, {"title": "Upgrade U (feat. Jay-Z)", "_id": "0GLUBbX4daHJkT3RQHEOia", "duration_ms": 273053, "preview_url": "https://p.scdn.co/mp3-preview/6a88d6460589303a3b4fb27fed848e5ce2f1aa1a?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 6, "album": {"name": "B'Day Deluxe Edition", "_id": "0Zd10MKN5j9KwUST0TdBBB"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "JAY-Z", "_id": "3nFkdlSjzX9mRTtwJOzDYB"}]}, {"title": "Beautiful Liar", "_id": "2HWWNoWEEEECwZhAiLg7ib", "duration_ms": 199853, "preview_url": "https://p.scdn.co/mp3-preview/4123e211fa9f615320415bba6faf4bfabd163e51?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "B'Day Deluxe Edition", "_id": "0Zd10MKN5j9KwUST0TdBBB"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "Shakira", "_id": "0EmeFodog0BfCgMzAIvKQp"}]}, {"title": "End of Time", "_id": "3FPUFltTjg6ClUL2wr9ux6", "duration_ms": 223986, "preview_url": "https://p.scdn.co/mp3-preview/14a554bb96c74422fe0a23a54993f63b4de13643?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 10, "album": {"name": "4", "_id": "1gIC63gC3B7o7FfpPACZQJ"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Deja Vu (feat. Jay-Z)", "_id": "423EcxblW9F4nnQkqcqMlK", "duration_ms": 240280, "preview_url": "https://p.scdn.co/mp3-preview/f99e1e07866947507b530ac6d225dc44c10e7571?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 13, "album": {"name": "B'Day Deluxe Edition", "_id": "0Zd10MKN5j9KwUST0TdBBB"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "JAY-Z", "_id": "3nFkdlSjzX9mRTtwJOzDYB"}]}, {"title": "Lift Off", "_id": "63dETSxIKCdUHGzlUyCjWb", "duration_ms": 266253, "preview_url": null, "track_number": 2, "album": {"name": "Watch The Throne (Deluxe)", "_id": "1YwzJz7CrV9fd9Qeb6oo1d"}, "artists": [{"name": "JAY-Z", "_id": "3nFkdlSjzX9mRTtwJOzDYB"}, {"name": "Kanye West", "_id": "5K4W6rqBFWDnAN6FQUkS6x"}, {"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "6 Inch (feat. The Weeknd)", "_id": "0pxNyJJiL2zZZ5GgJ4JhsQ", "duration_ms": 260440, "preview_url": "https://p.scdn.co/mp3-preview/4c37b4f1134701ab01d4b7159137adeab8e5fdad?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 5, "album": {"name": "Lemonade", "_id": "7dK54iZuOxXFarGhXwEXfF"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "The Weeknd", "_id": "1Xyo4u8uXC1ZmMpatF05PJ"}]}, {"title": "Pretty Hurts", "_id": "7lUA4P03AhwAw40JHkdyGr", "duration_ms": 257653, "preview_url": "https://p.scdn.co/mp3-preview/fd617af2cdf7ea1bebbe64ea170a2d34ea24f675?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 1, "album": {"name": "BEYONC\u00c9 [Platinum Edition]", "_id": "2UJwKSBUz6rtW4QLK74kQu"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Dance for You", "_id": "7cvkXf3AwPGT041PyOi5VX", "duration_ms": 377466, "preview_url": "https://p.scdn.co/mp3-preview/69d99b19f6557ff8cf42b354316ae883d7900347?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 6, "album": {"name": "4", "_id": "1gIC63gC3B7o7FfpPACZQJ"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}, {"title": "Freedom (feat. Kendrick Lamar)", "_id": "7aBxcRw77817BrkdPChAGY", "duration_ms": 289760, "preview_url": "https://p.scdn.co/mp3-preview/fbe70f454ebeab5073a4db690b1c1bbfcd396811?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 10, "album": {"name": "Lemonade", "_id": "7dK54iZuOxXFarGhXwEXfF"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "Kendrick Lamar", "_id": "2YZyLoL8N0Wb9xBt1NhZWg"}]}, {"title": "Shining (feat. Beyonc\u00e9 & Jay-Z)", "_id": "3OoZagIW3YK8vjt0ws6NOI", "duration_ms": 283253, "preview_url": "https://p.scdn.co/mp3-preview/65eef2abebaa79ef950340337b24ccf65429ac76?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "Grateful", "_id": "4JBZ0QHveEpESepanNBG8A"}, "artists": [{"name": "DJ Khaled", "_id": "0QHgL1lAIqAw0HtD7YldmP"}, {"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}, {"name": "JAY-Z", "_id": "3nFkdlSjzX9mRTtwJOzDYB"}]}, {"title": "Naughty Girl", "_id": "0YGQ3hZcRLC5YX7o0hdmHg", "duration_ms": 208573, "preview_url": "https://p.scdn.co/mp3-preview/300cc924f4c94a0450464f6d522867214e81e2cd?cid=9c6784a0ec4d4cb7a131d7192891e3f9", "track_number": 2, "album": {"name": "Dangerously In Love", "_id": "6oxVabMIqCMJRYN1GqR3Vf"}, "artists": [{"name": "Beyonc\u00e9", "_id": "6vWDO969PvNqNYHIOW5v0m"}]}]}''')

    db.insertChart(chart)

    db.insertPlaylist(playlist)
    db.insertTrack(track)

    db.getTrackByName("Crazy In Love (feat. Jay-Z)", "Beyonc\u00e9")

    x = db.collectPartialTracks()
    y = db.collectPartialAlbums()
    z = db.collectPartialArtists()

    db.insertArtist(artist)
    db.insertAlbum(album)

if __name__ == "__main__":
    app.run()
    #test()

